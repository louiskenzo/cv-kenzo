default: tex

docker:
	gitlab-ci-multi-runner exec docker compile

tex:
	xelatex -halt-on-error CV.en.tex
